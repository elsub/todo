<h3><?= $title ?></h3>
<form action="/todo/create">
    <div class="col-12">
    <?= \Config\Services::validation()->listErrors(); ?>
    </div>
    <div class="form-group">
        <label for="title">Title</label>
        <input class="form-control" name="title"
        placeholder="Enter title" maxlenght="255">
    </div>
    <div>
        <label for="description">Description</label>
        <textarea class="form-control" name="description"
        placeholder="Enter description"></textarea>
    </div>
    <button class="btn btn-info mt-3">Save</button>
    </form>