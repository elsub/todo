<h3><?= $title ?></h3>
<form action="/login/registration">
    <div class="col-12">
    <?= \Config\Services::validation()->listErrors(); ?>
    </div>
    <div class="form-group">
        <label>Username</label>
        <input class="form-control" name="user" placeholder="Enter username"
        maxlength="30"> 
    </div>
    <div class="form-group">
        <label>Firstname</label>
        <input class="form-control" 
        name="firstname" 
        placeholder="Enter firstname"
        maxlength="30"> 
    </div>
    <div class="form-group">
        <label>Lastname</label>
        <input class="form-control" 
        name="lastname" 
        placeholder="Enter lastname"
        maxlength="30"> 
    </div>
    <div class="form-group">
    <label>Password</label>
    <input class="form-control" name="password" type="password" placeholder="Enter password"
    maxlength="255">
    </div>
    <div class="form-group">
    <label>Password again</label>
    <input class="form-control" 
    name="passwordagain"
    type="password"
    placeholder="Enter password again"
    maxlength="255">
    </div>
    <button class="btn btn-info">Submit</button>
</form>