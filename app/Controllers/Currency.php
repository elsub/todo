<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\currencyModel;

    class Currency extends BaseController {

            public function index() {
                echo view("CurrencyView");
            }

            public function calculate() {
                // ladataan malli
                    $currencyModel = new currencyModel();

                    $dollar = $this->request->getVar("dollars");

                    print "dollars = $dollar";
                // välitetään rahasumma mallille
                    $euro = $currencyModel->calculateEuros($dollar);

                    // muotoillaan parametrit näkymälle
                    $data["title"] = "Dollars to Euros conversion";
                    $data["heading"] = "Results of conversion";
                    $data["dollarit"] = $dollar;
                    $data["eurot"] = $euro;

                    print "<p>Euros = $euro</p>";
                // ladata uusi näkymä, joka näyttää tulokset
                echo view("Views/showConversion", $data);
            }
            
    }
?>